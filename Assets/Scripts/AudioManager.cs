﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public static AudioManager singleton;

    public AudioMixerGroup defaultGroup;

    private List<AudioUnit> _audioUnits = new List<AudioUnit>();

    public AudioClip prepareClip;
    public AudioClip battleClip;
    public AudioClip aftermatchClip;
    private AudioSource _musicAudioSource;

    private void Awake()
    {
        if (singleton != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(gameObject);
            singleton = this;
        }
        _musicAudioSource = GetComponent<AudioSource>();
    }

    public void PlayMusic(AudioClip clip)
    {
        if (clip == null)
        {
            Debug.LogWarning("Missing audio clip", gameObject);
            return;
        }
        _musicAudioSource.clip = clip;
        _musicAudioSource.Play();
    }

    public void StopMusic()
    {
        _musicAudioSource.Stop();
    }

    public class AudioUnit
    {
        public Transform myTransform;
        public AudioSource audioSource;
        public float pitch = 1f;

        public AudioUnit()
        {
            myTransform = new GameObject("audioUnit").transform;
            myTransform.parent = singleton.transform;
            audioSource = myTransform.gameObject.AddComponent<AudioSource>();
            audioSource.spatialBlend = 1f;
            audioSource.minDistance = 30f;
        }
    }

    public void Play(AudioClip clip, Vector3 position, float pitchVariation, AudioMixerGroup group = null)
    {
        AudioUnit _unit = null;
        if (group == null)
        {
            group = defaultGroup;
        }

        // Try to find an unused audioUnit
        for (int i = 0; i < _audioUnits.Count; i++)
        {
            if (!_audioUnits[i].audioSource.isPlaying)
            {
                _unit = _audioUnits[i];
                break;
            }
        }

        // Else we create a new audioUnit
        if (_unit == null)
        {
            _unit = new AudioUnit();
            _audioUnits.Add(_unit);
        }

        if (pitchVariation != 0)
        {
            float variation = Random.Range(-pitchVariation, pitchVariation);
            _unit.audioSource.pitch = (1f + variation) * Time.timeScale;
            _unit.pitch = 1f + variation; // storing pitch for when we need to reset
        }
        else
        {
            _unit.audioSource.pitch = 1f;
        }
        _unit.audioSource.outputAudioMixerGroup = group;
        _unit.myTransform.position = position;
        _unit.audioSource.clip = clip;
        _unit.audioSource.Play();
    }

    // Change the pitch of every audioSource in the manager. Useful for slow-mo.
    public void SetAllAudioSpeed(float pitch)
    {
        for (int i = 0; i < _audioUnits.Count; i++)
        {
            _audioUnits[i].audioSource.pitch *= pitch;
        }
    }

    public void ResetAllAudioSpeed()
    {
        for (int i = 0; i < _audioUnits.Count; i++)
        {
            _audioUnits[i].audioSource.pitch = _audioUnits[i].pitch;
        }
    }
}