﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DropDetect : MonoBehaviour
{
    public float resetDistance = -20f;
    public float resetHoldTime = 1f;
    private float _resetHoldTimer = 0;

    private void Update()
    {
        if (transform.position.y < resetDistance)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (Input.GetKey(KeyCode.R))
        {
            _resetHoldTimer += Time.deltaTime;
            if (_resetHoldTimer >= resetHoldTime)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}