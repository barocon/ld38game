﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterMovement : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public LayerMask groundedLayers;

    private CharacterController _controller;
    private Vector3 moveDirection = Vector3.zero;
    private Animator _animator;
    private bool previousGrounded;

    public event Action footstepEvent;

    public event Action landingEvent;

    public event Action jumpEvent;

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (_controller.isGrounded)
        {
            if (!previousGrounded)
            {
                if (landingEvent != null)
                    landingEvent();
                previousGrounded = true;
            }

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpSpeed;
                if (jumpEvent != null)
                    jumpEvent();
            }
        }
        else
        {
            if (_controller.velocity.magnitude > 5f && !Physics.Raycast(transform.position, Vector3.down, 1f, groundedLayers))
            {
                Debug.DrawRay(transform.position, Vector3.down, Color.green, 2f);
                previousGrounded = false;
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;
        _controller.Move(moveDirection * Time.deltaTime);

        float cameraYrotation = Camera.main.transform.rotation.eulerAngles.y;
        Vector3 newCharacterRotation = transform.rotation.eulerAngles;
        newCharacterRotation.y = cameraYrotation;
        transform.rotation = Quaternion.Euler(newCharacterRotation);
        if (_animator)
            _animator.SetFloat("Speed", _controller.velocity.magnitude, 0.1f, Time.deltaTime);
    }

    public void CheckFootstep()
    {
        if (_controller.isGrounded && _controller.velocity.magnitude > 0.1f)
        {
            footstepEvent();
        }
    }
}