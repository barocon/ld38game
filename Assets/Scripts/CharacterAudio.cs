﻿using UnityEngine;
using UnityEngine.Audio;

public class CharacterAudio : MonoBehaviour
{
    public AudioClip[] footsteps;
    public AudioClip landingSound;
    public AudioClip jumpSound;
    public AudioClip liftSound;
    public AudioClip dropSound;

    public AudioMixerGroup mixerGroup;

    private CharacterMovement _cm;
    private GrabItems _gi;

    private void Start()
    {
        _cm = GetComponentInParent<CharacterMovement>();
        _gi = GetComponentInParent<GrabItems>();
        _cm.jumpEvent += PlayJumpSound;
        _cm.landingEvent += PlayLandingSound;
        _cm.footstepEvent += PlayFootstep;
        _gi.itemGrabbed += PlayLiftSound;
        _gi.itemDropped += PlaDropSound;
    }

    public void PlayLandingSound()
    {
        AudioManager.singleton.Play(landingSound, transform.position, 0.3f, mixerGroup);
    }

    public void PlayJumpSound()
    {
        AudioManager.singleton.Play(jumpSound, transform.position, 0.3f, mixerGroup);
    }

    public void PlayLiftSound()
    {
        AudioManager.singleton.Play(liftSound, transform.position, 0.1f);
    }

    public void PlaDropSound()
    {
        AudioManager.singleton.Play(dropSound, transform.position, 0.1f);
    }

    public void PlayFootstep()
    {
        int randomStep = Random.Range(0, footsteps.Length - 1);
        AudioManager.singleton.Play(footsteps[randomStep], transform.position, 0.3f, mixerGroup);
    }
}