using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPSCounter : MonoBehaviour
{
    public float fpsMeasurePeriod = 0.2f;
    public int maxFPS = 500;

    private int _accumulatedFPS;
    private float _nextFlushTime;
    private int _currentFPS;
    private string[] _fpsStrings;
    private float _previousFlushTime;

    private Text _textComponent;

    private bool _isPaused;

    private void Start()
    {
        _nextFlushTime = Time.realtimeSinceStartup + fpsMeasurePeriod;
        _previousFlushTime = Time.realtimeSinceStartup;
        _textComponent = GetComponent<Text>();

        _fpsStrings = new string[maxFPS + 1];
        for (int i = 0; i < maxFPS; i++)
        {
            _fpsStrings[i] = i.ToString() + " FPS\n " /*+ SystemInfo.graphicsDeviceVersion + "\n ShaderLevel " + SystemInfo.graphicsShaderLevel*/;
        }
        _fpsStrings[maxFPS] = maxFPS.ToString() + "+ FPS\n " /*+ SystemInfo.graphicsDeviceVersion + "\n ShaderLevel " + SystemInfo.graphicsShaderLevel*/;
    }

    private void Update()
    {
        if (_isPaused)
        {
            return;
        }

        _accumulatedFPS++;

        if (Time.realtimeSinceStartup >= _nextFlushTime)
        {
            float _timeBetween = Time.realtimeSinceStartup - _previousFlushTime;
            _currentFPS = (int)(_accumulatedFPS / _timeBetween);
            _accumulatedFPS = 0;
            _nextFlushTime += fpsMeasurePeriod;
            _previousFlushTime = Time.realtimeSinceStartup;

            //Debug.Log("FPS " + _currentFPS);
            if (_currentFPS <= maxFPS)
            {
                _textComponent.text = _fpsStrings[_currentFPS];
            }
            else
            {
                _textComponent.text = _fpsStrings[maxFPS];
            }
        }

        //if (QualityManager.singleton.highQuality && _currentFPS < 40)
        //{
        //    QualityManager.singleton.SetHighQualityEffects(false);
        //}
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        _isPaused = !hasFocus;
        _nextFlushTime = Time.realtimeSinceStartup + fpsMeasurePeriod;
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        _isPaused = pauseStatus;
        if (_textComponent)
            _textComponent.text = "paused".ToString();
    }
}