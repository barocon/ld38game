﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public static MenuController singleton;
    public GameObject pauseMenu;

    private void Start()
    {
        singleton = this;
        Continue();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)/* && !gameoverMenu.activeSelf && !sceneMenu.activeSelf*/)
        {
            //Debug.Log("Hit pause");
            if (pauseMenu.activeSelf)
                Continue();
            else
                Pause();
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Continue()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        //Player.singleton.GetComponent<UnityStandardAssets.Characters.FirstPerson.TopDownController>().enabled = true;
    }

    private void Pause()
    {
        Time.timeScale = 0f;

        pauseMenu.SetActive(true);
        //Player.singleton.GetComponent<UnityStandardAssets.Characters.FirstPerson.TopDownController>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }

    //public void ToggleSceneWindow(bool open)
    //{
    //    sceneMenu.SetActive(open);
    //    if (open)
    //    {
    //        pauseMenu.SetActive(false);
    //        gameoverMenu.SetActive(false);
    //    }
    //    else
    //    {
    //        pauseMenu.SetActive(true);
    //    }
    //}
}