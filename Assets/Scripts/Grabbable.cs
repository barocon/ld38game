﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{
    private List<Tile> touchingObjects = new List<Tile>();
    public Material highlightMaterial;
    private Material _oldMaterial;
    private Renderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<Renderer>();
        _oldMaterial = _renderer.material;
    }

    public void SetHightlightMaterial()
    {
        _renderer.material = highlightMaterial;
    }

    public void SetOldMaterial()
    {
        _renderer.material = _oldMaterial;
    }

    public void AddTouchingObject(Tile t)
    {
        touchingObjects.Add(t);
    }

    public void RemoveTouchingObject(Tile t)
    {
        touchingObjects.Remove(t);
    }

    public void ClearAllTouching()
    {
        for (int i = touchingObjects.Count - 1; i >= 0; i--)
        {
            touchingObjects[i].RemoveObject(gameObject);
        }
        //touchingObjects.Clear();
    }
}