﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private bool somethingIsTouching;
    private bool falling;
    private bool neverTouched = true;
    private List<GameObject> touchingObjects = new List<GameObject>();
    public Color touchingColor;
    public Color fallingColor;
    private new MeshRenderer renderer;
    private bool previouslyTouched;
    public const float leaveGracePeriod = 0.5f;
    private float _leaveTimer;
    public const float enterGracePeriod = 0.02f;
    private float _enterTimer;
    public LayerMask tileLayer;

    public AudioClip activated;
    public AudioClip startMoving;

    // Use this for initialization
    private void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        _enterTimer = enterGracePeriod;
    }

    // Update is called once per frame
    private void Update()
    {
        if (somethingIsTouching)
        {
            _enterTimer -= Time.deltaTime;

            if (_enterTimer > 0)
                return;

            if (neverTouched)
                neverTouched = false;
            if (falling)
                falling = false;

            if (!previouslyTouched)
            {
                // change color
                SetToColor(touchingColor);
                AudioManager.singleton.Play(activated, transform.position, 0.2f);
                previouslyTouched = true;
            }

            _leaveTimer = leaveGracePeriod;
        }
        else
        {
            _leaveTimer -= Time.deltaTime;
            _enterTimer = enterGracePeriod;
            previouslyTouched = false;
        }

        if (!falling && !neverTouched && !somethingIsTouching && _leaveTimer <= 0)
        {
            // fall
            SetToColor(fallingColor);
            falling = true;
            AudioManager.singleton.Play(startMoving, transform.position, 0.2f);
        }
        if (falling)
        {
            if (Physics.BoxCast(transform.position, Vector3.one * 0.5f, Vector3.down, transform.rotation, 0.51f, tileLayer))
            {
                Debug.DrawRay(transform.position, Vector3.down * 1.01f);
                //somethingIsTouching = true;
                return;
            }
            transform.Translate(Vector3.down * Time.deltaTime * 0.5f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        AddTouchedObject(collision.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        AddTouchedObject(other.gameObject);
    }

    private void AddTouchedObject(GameObject obj)
    {
        if (!touchingObjects.Contains(obj))
        {
            touchingObjects.Add(obj);
            somethingIsTouching = true;
            Grabbable grabbable = obj.GetComponent<Grabbable>();
            if (grabbable)
            {
                grabbable.AddTouchingObject(this);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        RemoveObject(collision.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        RemoveObject(other.gameObject);
    }

    public void RemoveObject(GameObject obj)
    {
        touchingObjects.Remove(obj);

        if (touchingObjects.Count == 0)
            somethingIsTouching = false;

        Grabbable grabbable = obj.GetComponent<Grabbable>();
        if (grabbable)
        {
            grabbable.RemoveTouchingObject(this);
        }
    }

    private void SetToColor(Color color)
    {
        TileHandler.props.SetColor("_Color", color);
        renderer.SetPropertyBlock(TileHandler.props);
    }
}