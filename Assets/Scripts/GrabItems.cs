﻿using System;
using UnityEngine;

public class GrabItems : MonoBehaviour
{
    private Rigidbody holdingItem;
    private Transform previousParent;
    public LayerMask itemLayers;
    private Grabbable _highlightTarget;

    public event Action itemGrabbed;

    public event Action itemDropped;

    private Rigidbody _rb;
    private SpringJoint _sj;
    private float distance;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _sj = GetComponent<SpringJoint>();
    }

    private void Update()
    {
        if (holdingItem)
        {
            distance = Mathf.Clamp(distance + Input.GetAxis("Mouse ScrollWheel") * 5f, 0, 5f);
            //_sj.anchor = new Vector3(0, 0, distance);
            Vector3 newAnchor = _sj.anchor;
            newAnchor.z = distance;
            _sj.anchor = newAnchor;
            if (Input.GetMouseButton(1))
            {
                _sj.connectedBody.AddTorque(new Vector3(-Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0) * 50f);
            }
        }
        if (holdingItem == null)
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position - transform.forward, 0.8f, Camera.main.transform.forward, out hit, 3f, itemLayers))
            {
                Debug.DrawRay(transform.position, transform.forward * 3f);
                //Debug.Log("hit " + hit.collider.name);
                var rb = hit.collider.GetComponent<Rigidbody>();
                if (rb)
                {
                    Grabbable grabbable = rb.GetComponent<Grabbable>();
                    grabbable.SetHightlightMaterial();
                    if (_highlightTarget != grabbable && _highlightTarget != null)
                        _highlightTarget.SetOldMaterial();
                    _highlightTarget = grabbable;
                    if (Input.GetButtonDown("Fire1"))
                    {
                        rb.useGravity = false;
                        //rb.isKinematic = true;
                        //hit.collider.enabled = false;
                        _sj.connectedBody = rb;
                        distance = Vector3.Distance(transform.position, rb.position);
                        rb.drag = 50f;
                        rb.angularDrag = 5f;
                        holdingItem = rb;
                        previousParent = rb.transform.parent;
                        rb.transform.parent = transform;
                        //Debug.Log("holding " + hit.collider.name);
                        grabbable.ClearAllTouching();
                        if (itemGrabbed != null)
                            itemGrabbed();
                    }
                }
                return;
            }
        }
        if (Input.GetButtonDown("Fire1") && holdingItem)
        {
            holdingItem.useGravity = true;
            holdingItem.isKinematic = false;
            holdingItem.GetComponent<Collider>().enabled = true;
            holdingItem.transform.parent = previousParent;
            Grabbable grabbable = holdingItem.GetComponent<Grabbable>();
            grabbable.SetOldMaterial();

            _sj.connectedBody = null;
            holdingItem.drag = 0;
            holdingItem.angularDrag = 0.15f;

            if (itemDropped != null)
                itemDropped();

            holdingItem = null;
        }
        if (_highlightTarget)
        {
            _highlightTarget.SetOldMaterial();
            _highlightTarget = null;
        }
    }
}