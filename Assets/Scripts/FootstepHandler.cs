﻿using UnityEngine;

public class FootstepHandler : MonoBehaviour
{
    private CharacterMovement _cm;

    private void Start()
    {
        _cm = GetComponentInParent<CharacterMovement>();
    }

    public void Footstep()
    {
        _cm.CheckFootstep();
    }
}