﻿using UnityEngine;

public class QualityManager : MonoBehaviour
{
    public MonoBehaviour[] highQualityEffects;

    public static QualityManager singleton;

    public bool highQuality = true;

    private void Awake()
    {
        singleton = this;
        Application.targetFrameRate = 140;

        //if (SystemInfo.graphicsDeviceVersion.Contains("OpenGL ES 2") || SystemInfo.graphicsShaderLevel < 30)
        //{
        //    QualitySettings.SetQualityLevel(0);
        //}
        //else
        //{
        //    QualitySettings.SetQualityLevel(1);
        //}

        if (QualitySettings.GetQualityLevel() == 0)
        {
            SetHighQualityEffects(false);
        }
    }

    public void SetHighQualityEffects(bool setOn)
    {
        foreach (var fx in highQualityEffects)
        {
            fx.enabled = setOn;
        }
        highQuality = setOn;
    }
}