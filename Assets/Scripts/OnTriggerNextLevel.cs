﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OnTriggerNextLevel : MonoBehaviour
{
    public AudioClip victory;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharacterController>())
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            AudioManager.singleton.Play(victory, transform.position, 0);
        }
    }
}